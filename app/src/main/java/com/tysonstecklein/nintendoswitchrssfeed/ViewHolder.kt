package com.tysonstecklein.nintendoswitchrssfeed

import android.view.View
import android.widget.ImageView
import android.widget.TextView

/**
* Created by Tyson Stecklein on 1/21/2018 at 3:04 PM.
*/
class ViewHolder(view: View) {
    val title: TextView = view.findViewById(R.id.list_item_container_title)
    val link: TextView = view.findViewById(R.id.list_item_container_link)
    val image: ImageView = view.findViewById(R.id.list_item_container_image)
}