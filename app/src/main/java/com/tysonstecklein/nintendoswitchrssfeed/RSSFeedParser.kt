package com.tysonstecklein.nintendoswitchrssfeed

import android.util.Log
import org.xmlpull.v1.XmlPullParser
import org.xmlpull.v1.XmlPullParserFactory

private const val KEY_ITEM = "item"
private const val KEY_DESCRIPTION = "description"
private const val KEY_TITLE = "title"
private const val KEY_LINK = "link"
private const val TAG = "RSSFeedParser"

class RSSFeedParser {
    val rssEntries = ArrayList<FeedItem>()

    fun parse(xmlData: String, feedLimit: Int): Boolean {
        Log.d(TAG, "parse called with $xmlData")
        var status = true
        var inItem = false
        var textValue = ""
        var currentFeedItemCount = 0
        Log.d(TAG, "selected feed limit: $feedLimit")
        try {
            val factory = XmlPullParserFactory.newInstance()
            factory.isNamespaceAware = true
            val xmlPullParser = factory.newPullParser()
            xmlPullParser.setInput(xmlData.reader())
            var eventType = xmlPullParser.eventType
            var currentRecord = FeedItem()
            while (eventType != XmlPullParser.END_DOCUMENT) {
                val tagName = xmlPullParser.name?.toLowerCase()
                when (eventType) {
                    XmlPullParser.START_TAG -> {
                        Log.d(TAG, "parse: START_TAG: $tagName")
                        if (tagName == "item") {
                            inItem = true
                        }
                    }

                    XmlPullParser.TEXT -> {
                        textValue = xmlPullParser.text
                        Log.d(TAG, "parse: TEXT: $textValue")
                    }

                    XmlPullParser.CDSECT -> {
                        textValue = xmlPullParser.text
                        Log.d(TAG, "parse: CDSECT: $textValue")
                    }

                    XmlPullParser.END_TAG -> {
                        Log.d(TAG, "parse: END_TAG: $tagName")
                        if (inItem) {
                            when (tagName) {
                                KEY_ITEM -> {
                                    if (currentFeedItemCount < feedLimit) {
                                        rssEntries.add(currentRecord)
                                        currentFeedItemCount++
                                        inItem = false
                                        currentRecord = FeedItem() // create new object
                                    }
                                }
                                KEY_TITLE -> currentRecord.title = textValue
                                KEY_DESCRIPTION -> currentRecord.imageURL = parseImageURL(textValue)
                                KEY_LINK -> currentRecord.linkURL = textValue
                            }
                        }
                    }
                }
                eventType = xmlPullParser.nextToken()
            }

//            for (app in rssEntries) {
//                Log.d(TAG, "***************************")
//                Log.d(TAG, app.toString())
//            }

        } catch (e: Exception) {
            e.printStackTrace()
            status = false
        }

        return status
    }

    private fun parseImageURL(description: String): String {
        val imageURL = StringBuilder()
        val keyCharSequence = "img src=\""
        val sequenceOffset = keyCharSequence.length
        var index = description.indexOf(keyCharSequence)
        Log.d("TBONEparse", "index of $keyCharSequence found at $index | offset value: $sequenceOffset")
        Log.d("TBONEparse", "description: $description")
        var insideQuotes = true
        if (index >= 0) {
            index += sequenceOffset
            while (insideQuotes) {
                imageURL.append(description[index])
                index++
                if(description[index].toString() == "\"") {
                    insideQuotes = false
                }
            }
        } else {
            return description
        }

        return imageURL.toString()
    }
}