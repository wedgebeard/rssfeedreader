package com.tysonstecklein.nintendoswitchrssfeed

import android.content.Context
import android.os.AsyncTask
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.ListView
import kotlinx.android.synthetic.main.activity_main.*
import java.net.URL
import kotlin.properties.Delegates

private const val TAG = "MainActivity"

class MainActivity : AppCompatActivity() {
    private var _downloadData : DownloadData? = null
    private var feedUrl: String = ""
    private var feedLimit: Int = 10

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        feedUrl = "https://www.nintendo.com/feed"
        downloadUrl(feedUrl)
    }

    private fun downloadUrl(feedUrl: String) {
        Log.d(TAG, "downloadUrl: called")
        _downloadData = DownloadData(this, xmlListView, feedLimit)
        _downloadData?.execute(feedUrl)
        Log.d(TAG, "downloadUrl: done")
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.feeds_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        when (item.itemId) {
            R.id.menu_nintendo -> {
                feedUrl = "https://www.nintendo.com/feed"
            }
            R.id.menu_nintendolife -> {
                feedUrl = "http://www.nintendolife.com/feeds/latest"
            }
            R.id.menu_steam -> {
                feedUrl = "http://store.steampowered.com/feeds/news.xml"
            }
            R.id.menu_10 -> {
                if(!item.isChecked) {
                    feedLimit = 10
                    item.isChecked = true
                }
            }
            R.id.menu_25 -> {
                if (!item.isChecked) {
                    item.isChecked = true
                    feedLimit = 25
                }
            }
            R.id.menu_50 -> {
                if (!item.isChecked) {
                    item.isChecked = true
                    feedLimit = 50
                }
            }
            else -> {
                return super.onOptionsItemSelected(item)
            }
        }

        Log.d(TAG, "current feedURL: $feedUrl")
        downloadUrl(feedUrl)
        return true
    }

    override fun onDestroy() {
        super.onDestroy()
        _downloadData?.cancel(true)
    }

    companion object {
        private class DownloadData(context: Context, listView: ListView, var feedLimit: Int) : AsyncTask<String, Void, String>() {
            private val _tag = "DownloadData"
            private var propertyContext : Context by Delegates.notNull()
            private var propertyListView : ListView by Delegates.notNull()

            init {
                propertyContext = context
                propertyListView = listView
            }

            override fun onPostExecute(result: String) {
                super.onPostExecute(result)
                val rssFeedParser = RSSFeedParser()
                Log.d(TAG, "onPostExecute: TBONE")
                rssFeedParser.parse(result, feedLimit)

//region default feedAdapter
//                val arrayAdapter = ArrayAdapter<FeedItem>(propertyContext, R.layout.list_item, rssFeedParser.rssEntries)
//                propertyListView.adapter = arrayAdapter
//endregion default feedAdapter

                val feedAdapter = FeedAdapter(propertyContext, R.layout.list_item_container, rssFeedParser.rssEntries)
                propertyListView.adapter = feedAdapter

            }

            override fun doInBackground(vararg url: String?): String {
                Log.d(_tag, "doInBackground: starts with ${url[0]}")
                val rssFeed = downloadXML(url[0])
                if (rssFeed.isEmpty()) {
                    Log.e(_tag, "doInBackground: Error downloading url: ${url[0]}")
                }
                return rssFeed
            }

            private fun downloadXML(urlPath: String?): String {
                return URL(urlPath).readText()
            }
//region old downloadXML function
//            private fun downloadXML(urlPath: String?): String {
//                val xmlResult = StringBuilder()
//
//                try {
//                    val url = URL(urlPath)
//                    val connection: HttpURLConnection = url.openConnection() as HttpURLConnection
//                    val response = connection.responseCode
//                    Log.d(TAG, "downloadXML: The response code was $response")

//region handling reading URL step by step
                    //            val inputStream = connection.inputStream
                    //            val inputStreamReader = InputStreamReader(inputStream)
                    //            val reader = BufferedReader(inputStreamReader)
//endregion handling reading URL step by step

//region java style buffered reader
//                    val reader = BufferedReader(InputStreamReader(connection.inputStream))
//                    val inputBuffer = CharArray(500)
//                    var charsRead = 0
//                    while (charsRead >= 0) {
//                        charsRead = reader.read(inputBuffer)
//                        if (charsRead > 0) {
//                            xmlResult.append(String(inputBuffer, 0, charsRead))
//                        }
//                    }
//                    reader.close()
//endregion java style buffered reader

//                    var stream = connection.inputStream
//                    stream.buffered().reader().use { xmlResult.append(it.readText()) }
//
//                    Log.d(TAG, "Received ${xmlResult.length} bytes")
//                    return xmlResult.toString()
//                } catch (e : Exception) {
//                    val errorMessage: String = when (e) {
//                        is MalformedURLException -> "downloadXML: Invalid URL ${e.message}"
//                        is IOException -> "downloadXML: IO Exception reading data: ${e.message}"
//                        is SecurityException -> {
//                            e.printStackTrace()
//                            "downloadXML: Security exception. Needs permissions? ${e.message}"
//                        }
//                        else -> "downloadXML: General Error exception: ${e.message}"
//                    }
//                }
//region exception handling java style
//                } catch (e : MalformedURLException){
//                    Log.e(TAG, "downloadXML: Invalid URL ${e.message}")
//                } catch (e : IOException) {
//                    Log.e(TAG, "downloadXML: IO Exception reading data: ${e.message}")
//                } catch (e : SecurityException) {
//                    e.printStackTrace()
//                    Log.e(TAG, "downloadXML: Security exception. Needs permissions? ${e.message}")
//                } catch (e : Exception) {
//                    Log.e(TAG, "downloadXML: General Error exception: ${e.message}")
//                }
//endregion exception handling java style
//                return "" // If it gets this far, there's been a problem, return empty string
//            }
//endregion old downloadXML function

        }
    }
}
