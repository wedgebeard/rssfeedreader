package com.tysonstecklein.nintendoswitchrssfeed

/**
 * Created by Sorbo on 1/15/2018.
 */

class FeedItem {
    var title: String = ""
    var linkURL: String = ""
    var imageURL: String = ""

    override fun toString(): String {
        return """
            title: $title
            linkURL: $linkURL
            imageURL: $imageURL
            """.trimIndent()
    }
}