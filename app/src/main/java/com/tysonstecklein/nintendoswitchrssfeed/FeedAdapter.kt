package com.tysonstecklein.nintendoswitchrssfeed

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter

//private const val TAG = "FeedAdapter"

class FeedAdapter(context: Context, private val resource: Int, private val items: List<FeedItem>)
    : ArrayAdapter<FeedItem>(context, resource) {

    private val inflater = LayoutInflater.from(context)

    override fun getCount(): Int {
//        Log.d(TAG, "getCount called: ${items.count()}")
        return items.count()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {

//        Log.d(TAG, "getView called")
        val view: View
        val viewHolder: ViewHolder

        if (convertView == null) {
//            Log.d(TAG, "getView called: creating new view")
            view = inflater.inflate(resource, parent, false)
            viewHolder = ViewHolder(view)
            view.tag = viewHolder
        } else {
//            Log.d(TAG, "getView called: reusing convertView")
            view = convertView
            viewHolder = view.tag as ViewHolder
        }

//        val view = inflater.inflate(resource, parent, false) //this is expensive to memory

//        val title: TextView = view.findViewById(R.id.list_item_container_title)
//        val link: TextView = view.findViewById(R.id.list_item_container_link)
//        val image: ImageView = view.findViewById(R.id.list_item_container_image)


        val currentItem = items[position]

//        list_item_container // not sure what i was trying to do here
        viewHolder.title.text = currentItem.title
        viewHolder.link.text = currentItem.linkURL
//        image.setImageBitmap(currentItem.imageURL)

        return view
    }
}